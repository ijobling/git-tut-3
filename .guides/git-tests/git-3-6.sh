#!/bin/bash
QCOUNT=2

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Keep the master branch content only" "Reproducing a Git conflict in order to understand it" 'grep -v "<<<<<<<" /home/codio/workspace/conflict/conflict.txt'
				;;
      2 ) 
        expect "Commit the changes with the specified message" "Conflict solved: left master" 'git log -1'
        ;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command