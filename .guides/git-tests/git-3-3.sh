#!/bin/bash
QCOUNT=4

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Create a file 'merge.txt' in the 'merge' folder adding the word: Thaipoe" "Thaipoe" "grep Thaipoe /home/codio/workspace/merge/merge.txt"
				;;
      2 )
				expect "Stage and commit the changes adding the requested message" "diff --git a/merge/merge.txt b/merge/merge.txt" "git log -p -1"
				;;
      3 )
				expect "Stage and commit the changes adding the requested message" "Added merge.txt file" "git log -p -1"
				;;
			4 )
				expect "Create and switch to a 'merge' branch" "\*[[:space:]]*merge" "git branch"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command