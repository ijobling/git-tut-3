#!/bin/bash
QCOUNT=4

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Switch to the 'master' branch and merge the 'merge' branch to it. Make sure you changed 'Thaipoe' to 'Typo'" "\*[[:space:]]*master" "git branch"
				;;
      2 )
				expect "Change Thaipoe to Typo in the 'merge.txt' file and merge the changes to the 'master' branch " "Typo" "grep Typo /home/codio/workspace/merge/merge.txt"
				;;
      3 )
				expect "Stage and commit the changes adding the requested message" "diff --git a/merge/merge.txt b/merge/merge.txt" "git log -p -1"
				;;
      4 )
				expect "Stage and commit the changes adding the requested message" "Edited merge.txt file" "git log -p -1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command