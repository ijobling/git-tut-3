#!/bin/bash
QCOUNT=1

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create and switch to an update branch" "\*[[:space:]]*update" "git branch"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command