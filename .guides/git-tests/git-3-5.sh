#!/bin/bash
QCOUNT=5

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Create a feature branch" ">>>>>>> feature" "cat /home/codio/workspace/conflict/conflict.txt"
				;;
      2 )
				expect "Recreate the conflict above" "<<<<<<< HEAD" "cat /home/codio/workspace/conflict/conflict.txt"
				;;
      3 )
				expect "Text in master branch file does not match the requested text" "Reproducing a Git conflict in order to understand it" "cat /home/codio/workspace/conflict/conflict.txt"
				;;
      4 )
				expect "Text in feature branch file does not match the requested text" "Reproducing a Git conflict to understand it" "cat /home/codio/workspace/conflict/conflict.txt"
				;;
      5 )
				expect "Merge the feature branch into the master branch to recreate the conflict" ">>>>>>> feature" "cat /home/codio/workspace/conflict/conflict.txt"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command