@annotation:tour intro
#A POETIC INTRODUCTION TO GIT

#Module 3 - Branching

Branching is one of Git's most powerful features. Working in branches allows you to fully isolate each bug or feature you’re working on from your master branch and from other features and bug fixes you may be working on.

We’ll work through a couple of use cases shortly, but first let’s understand the reasons why branching is needed.

@annotation:tour why
#Why is branching important
One day you wake up and decide to work on a new feature in your code. It will probably take you a week or two to build this feature. Bursting with developer passion, you start coding away. 

5 days later, someone reports an urgent bug that is threatening your product. The problem is that your code now has a half developed feature that means the whole thing is completely unstable and so fixing your bug is not possible.

With branching, you solve this issue by doing each feature and each bug fix in its own **branch**. Of course, you could fix 3 or 4 bugs in a single branch but to be really structured it is better to do one per branch.

You happen to be in your master branch, which is the only place you know about at this stage and is where all your work has been done until now.

Branching allows you to branch off from a clean starting point into a separate space where you code can without affecting your master branch.

@annotation:tour cleanstart
#A clean start
The project in this module comes with a simple project set up and with some commits already made, so everything is fully prepared for you.

Enter the following on the command line in your terminal to see the commits already made at the project status.
```
git log
git status
```

This is a picture of what has been committed so far, with the most recent commit and the top.

![](img/commits-1.png)


@annotation:tour masterbranch
#The master branch
One of the things you’ll notice when you run a `git status` is that it will say

```
# On branch master
```

You should think of the master branch as the place where your code lives in its purest, most production ready state. Git always creates the master branch with any new repo.

Here are some guidelines

- you should avoid doing any development in the master branch
- don't create new features in the master branch
- don't fix bugs in the master branch, generally speaking you should create a new branch for each bug fix
- master is pure, master is holy and should be protected (maybe going a little far, just trying to make the point)


@annotation:tour sonnet
#Adding a Shakespeare Sonnet
We are currently on the master branch. We are going to add our first sonnet into the master branch. Having just lectured you about how bad this you may wonder why. The reason is to show you how Git will help you correct your transgression.

Add and stage (but don’t commit) the following famous Shakespeare sonnet 18.

- Create a file `sonnets.txt`
- In that file, write the sonnet. If you have forgotten it, you can copy and paste it from just below.
- Stage the new file with `git add sonnets.txt` or `git add -A`

```
Shall I compare thee to a summer’s day?
Thou art more lovely and more temperate.
Rough winds do shake the darling buds of May,
And summer’s lease hath all too short a date.
Sometime too hot the eye of heaven shines,
And often is his gold complexion dimmed;
And every fair from fair sometime declines,
By chance, or nature’s changing course, untrimmed;
But thy eternal summer shall not fade,
Nor lose possession of that fair thou ow’st,
Nor shall death brag thou wand’rest in his shade,
When in eternal lines to Time thou grow’st.
     So long as men can breathe, or eyes can see,
     So long lives this, and this gives life to thee.
```

@annotation:tour branchtime
#Branch Out
I mentioned that Git will help you out if you start your modifications in the master branch. If we now create a new branch, Git will take everything over from your working directory, including untracked and unstaged files.

```
git branch sonnets
```

You won’t see anything happen on the screen, but now enter the following

```
git branch
```

and you’ll see you have two branches. 

```
* master
  sonnets
```

You are currently on the `master` branch as it has the `*` character next to it but you can see the `sonnets` branch has been successfully created.

If you now were to perform Git operations such as `git add` and `git commit`, they would be added to the `master` branch, as we have not yet switched over to the ‘sonnets’ branch.

@annotation:tour checkout
#Switch Branch
The reason we already added some files to the master branch is that I want to show that when you switch to a new branch (checkout a branch in Git parlance), Git will move all uncommitted changes from tracked and untracked files into your new branch. 

Let’s have a go

```
git checkout sonnets
```

Now get a list of all branches again

```
git branch
```

```
  master
* sonnets
```

We can see we're now on the `sonnets` branch. In Codio, you can also see the current branch by looking at the top of the Codio file tree, where you’ll see the branch name in parentheses next to the project name `git-tut-module-3 (master)`.

##Commit what we brought across
Feel free to add more sonnets or leave things as they are. Either way, stage (`git add`) and commit the changes in your new `sonnets` branch.

Here's a little visualization.

![](img/commits-sonnets.png)

@annotation:tour bugfix
#Urgent Bug Fix
You get an urgent message that someone has discovered a nasty bug that needs immediate attention. You’re in the middle of working on your new sonnets but we can use Git branching.

The first thing we want to do is to go back to the clean code state that we would expect to find in the master branch.

**Make sure all your changes are committed in the `sonnets` branch** then let’s switch back to the master branch

```
git checkout master
```

Look what’s happened to `sonnets.txt`. It’s gone. That’s because it was not there in the master branch. 

But `sonnets.txt` is safely in the `sonnets` branch of course.

You can switch back and forth between `master` and `sonnets` but end up in `master`.

Here's another visualization of the state now we have switched back to master. The dotted circle at the top represents where we currently are. You can see is joined to the 'Verse 3 added' commit, which is the latest point in the master branch.

The `sonnets` branch has effectively been parked until we want to go back to it.

![](img/commits-back-to-master.png)

@annotation:tour onyourown
#Bug fix - you’re on your own
I’m not going to walk you through this step by step. You will hopefully remember the steps that you need to accomplish from the previous section.

This is what you should do

- in the first line of the third verse of `mary.txt` there is a typo (‘turnd’). 
- create a new branch called `fixv3`
- in that branch (don’t forget to switch to it), correct the typo by changing it to ‘turned’ 
- commit the changes 
- return to the master branch.

You should at this point still see the typo but if you checkout the `fixv3` branch again, you will see the correction.

That’s it. Now we’re ready to merge the bug fix into our `master` branch so we can release it.

@annotation:tour merge
#Merging them together
Merging takes a specified branch and merges it into the current branch.

In our example, we want to merge *into* the `master` branch, so make sure you are in the `master` branch (`git branch` will tell you). 

Now we merge in our `fixv3` branch with

```
git merge fixv3
```

Now, take a look at `mary.txt`. You can see that the typo is fixed.

@annotation:tour sonnets2
#Completing our Sonnets
Now, let’s get back to our sonnets. By now you are hopefully comfortable with the process, so I’ll just give you the high level tasks.

First, checkout your `sonnets` branch then add each of the following Sonnets then stage and commit each one individually. It is not the end of the world if you don’t, but we want to end up as close to the video as possible.

```
SONNET 116
Let me not to the marriage of true minds
Admit impediments. Love is not love
Which alters when it alteration finds,
Or bends with the remover to remove:
O no; it is an ever-fixed mark, 
That looks on tempests, and is never shaken;
It is the star to every wandering bark,
Whose worth's unknown, although his height be taken.
Love's not Time's fool, though rosy lips and cheeks 
Within his bending sickle's compass come; 
Love alters not with his brief hours and weeks, 
But bears it out even to the edge of doom.
   If this be error and upon me proved,
   I never writ, nor no man ever loved. 
```

```
SONNET 19
Devouring Time, blunt thou the lion's paws,
And make the earth devour her own sweet brood;
Pluck the keen teeth from the fierce tiger's jaws,
And burn the long-lived phoenix in her blood; 
Make glad and sorry seasons as thou fleet'st, 
And do whate'er thou wilt, swift-footed Time, 
To the wide world and all her fading sweets; 
But I forbid thee one most heinous crime: 
O, carve not with thy hours my love's fair brow,
Nor draw no lines there with thine antique pen; 
Him in thy course untainted do allow 
For beauty's pattern to succeeding men. 
Yet, do thy worst, old Time: despite thy wrong,
My love shall in my verse ever live young. 
```

@annotation:tour mergesonnets
#Merging the Sonnets
Once you have committed the sonnets, switch back to the master branch and merge in the `sonnets` branch.

By now, I hope you’re comfortable with the basics of branches and merging. There are a few more things worth knowing.

Section - Deleting the merged branches
Now that we have merged everything in, we can remove the other branches. This is done with 

```
git branch -d sonnets
git branch -d fixv3
```

@annotation:tour conflictres
#Conflict Resolution
Git is very clever at merging branches. As long as it is confident that it can merge not just files but also file contents together, it will do so.

However, if the same file has been edited in the same place in two branches undergoing a merge, then Git raises the white flag and will ask you do resolve the conflict yourself.

Let’s create this situation and deal with it. We’ll do this by creating a new branch

`git branch conflict`

Now switch to that branch and edit the poem by adding ‘blah blah’ to the end of the first line. Also, add the following verse to the end of the poem

```
It followed her to school one day
School one day, school one day
It followed her to school one day
Which was against the rules.
```

Commit these changes (you should know how by now) and then switch back to the `master` branch

`git checkout master`

Now, in the master branch add ‘waffle’ to the end of the first line of the poem. Commit these changes, too.

We’ve now created a situation where the same line in a file has been edited in both branches. Let’s merge in the changes we made in the `conflict` branch with 

`git merge conflict`

Git realizes it can’t resolve this on its own, so you will see the following output in the terminal

```
Auto-merging mary.txt                                                                                                          
CONFLICT (content): Merge conflict in mary.txt                                                                                 
Automatic merge failed; fix conflicts and then commit the result. 
```

If you run `git status` you’ll also see warnings there.

Now look at mary.txt and you can see the actual conflict embedded into the first verse of the poem

```
<<<<<<< HEAD
Mary had a little lamb, waffle
=======
Mary had a little lamb, blah blah
>>>>>>> conflct
Little lamb, little lamb,
Mary had a little lamb,
Its fleece was white as snow
```

What you now need to do is to resolve this conflict by editing the file. `<<<<< HEAD` means that everything from that point onwards until the `======` is contained within the branch you are trying to merge into (`master`).

Lower down, you’ll see `>>>>> conflict`, so everything above that up to the ‘======‘ is contained within the `conflict` branch.

All you now have to do is to edit the file to remove the section you don’t want to keep. If we want to keep the changes in the `conflict` branch, it will look like this after editing

```
Mary had a little lamb, blah blah
His fleece was white as snow,
And everywhere that Mary went,
The lamb was sure to go.
```

Although we have fixed the correction, we now have to perform a commit in order to store it properly

`git commit -am 'fixed the conflict'`

@annotation:tour stash
#Stashing
Let’s say you have been working on some code in a branch and then you want to quickly switch to another branch *but* you don’t want to commit your code changes because they are not really in a commit-worthy state and you don’t want to have  messy commits in your commit history.

Stashing does the following : 

1. It stores your dirty working directory code changes in a temporary storage area known as the Stash.
1. It then rolls back your working directory to the most recent commit. 

You can now switch to another branch without taking over those changes.

When you come back to the original branch, you can ‘pop’ your changes off the stash stack which applies those stored changes back to your working directory.

@annotation:tour stash
#Trying out git stash
Let’s try this out in your Codio Box.

First of all, make sure you have committed everything so we have a clean starting point. 

1. Add a new file called `temp.txt`
1. Stage this file `git add temp.txt` for a single file or `git add -A` if you have have several new files
1. Run `git stash` to stash your changes

As soon as you run the `git stash` command, you will notice that `temp.txt` has disappeared. This is because `git stash` caused Git to roll back to the previous commit, where `temp.txt` did not exist.

We can check what is on the stack with `git stash list`. You should see something like

```
stash@{0}: WIP on master: a0d2647 commit message shown here  
```

This would now enable you do switch to another branch in a clean state and without your dirty code changes.

Once you get back to your `master` branch, you can bring the stashed changes back with

```
git stash pop
```

You’ll notice that `temp.txt` has been restored and you are back to the exact same state you were before you stashed.

```
git stash list
```

will show that there is nothing more on the stack. If you had not popped the stash from the stack then you would see your stashed work listed.



@annotation:tour play
#Play
You should now play around with branching and merging until you feel really confident about the process. Make sure you create conflicts and resolve them and try creating multiple branches in parallel before merging them in different ways.

Once this process has become second nature, move on to the next section.




