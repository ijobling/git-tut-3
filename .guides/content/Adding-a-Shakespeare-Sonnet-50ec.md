We are currently on the master branch. We are going to add our first sonnet into the master branch. Having just lectured you about how bad this you may wonder why. The reason is to show you how Git will help you correct your transgression.

Add and stage (but don’t commit) the following famous Shakespeare sonnet 18.

- Create a file `sonnets.txt` in the `sonnets/` directory
- In that file, write the sonnet. If you have forgotten it, you can copy and paste it from just below.
- Stage the new file with `git add sonnets/sonnets.txt` or `git add -A`

Important: Notice that we are not committing the `sonnets.txt` file yet, we are just _staging_ it.

```
Shall I compare thee to a summer’s day?
Thou art more lovely and more temperate.
Rough winds do shake the darling buds of May,
And summer’s lease hath all too short a date.
Sometime too hot the eye of heaven shines,
And often is his gold complexion dimmed;
And every fair from fair sometime declines,
By chance, or nature’s changing course, untrimmed;
But thy eternal summer shall not fade,
Nor lose possession of that fair thou ow’st,
Nor shall death brag thou wand’rest in his shade,
When in eternal lines to Time thou grow’st.
     So long as men can breathe, or eyes can see,
     So long lives this, and this gives life to thee.
     
```