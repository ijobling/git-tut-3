[Click here to close files opened in previous section](close_file sonnets/sonnet-116.txt sonnets/sonnet-19.txt)

Once you have committed the sonnets, switch back to the `master` branch and merge in the `sonnets` branch.

### Branch ahead message

The `sonnets` branch was created from `master` and Git knows that you committed changes in the `sonnets` branch that outdated `master`. Once you merge the latest commits from `sonnets` this message should go away.


### Welcome to the _nano_ window

After executing the `git merge sonnets` command, you'll come across this terminal screen:

![nano](.guides/img/nano-1.png)

This is the _nano_ editor prompting you for confirmation. In order to confirm just press `ctrl + x` as stated in the nano editor footer (`^X Exit`): 

![nano-footer](.guides/img/nano-footer.png)

By now, I hope you’re comfortable with the basics of branches and merging. There are a few more things worth knowing.

---
Complete a set of challenges about `git merge` in the next section.