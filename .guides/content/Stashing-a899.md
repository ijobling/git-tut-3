Let’s say you have been working on some code in a branch and then you want to quickly switch to another branch *but* you don’t want to commit your code changes because they are not really in a commit-worthy state and you don’t want to have  messy commits in your commit history.

Stashing does the following : 

1. It stores your dirty working directory code changes in a temporary storage area known as the Stash.
1. It then rolls back your working directory to the most recent commit. 

You can now switch to another branch without taking over those changes.

When you come back to the original branch, you can ‘pop’ your changes off the stash stack which applies those stored changes back to your working directory.