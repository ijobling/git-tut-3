You should now play around with branching and merging until you feel really confident about the process. Make sure you create conflicts and resolve them and try creating multiple branches in parallel before merging them in different ways.

Once this process has become second nature, move on to the next unit.
