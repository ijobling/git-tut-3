I’m not going to walk you through this step by step. You will hopefully remember the steps that you need to accomplish from the previous section.

This is what you should do

- in the first line of the third verse of `mary.txt` there is a typo (‘turnd’). 
- create a new branch called `fixv3`
- in that branch (don’t forget to switch to it), correct the typo by changing it to ‘turned’ 
- commit the changes 
- return to the `master` branch.

You should at this point still see the typo but if you checkout the `fixv3` branch again, you will see the correction.

That’s it. Now we’re ready to merge the bug fix into our `master` branch so we can release it.