{Check It!|assessment}(test-1591754334)

|||guidance

### Correct answers:

Make sure you've completed the previous challenge. Doing a `git branch` should display the `merge` branch as checked out: `* merge`.

1. Change the word _Thaipoe_ in the `merge/merge.txt` to _Typo_
2. `git add -A`, `git commit -m "Edited merge.txt file"`
3. `git checkout master`, `git merge merge`

|||