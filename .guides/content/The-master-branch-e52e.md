One of the things you’ll notice when you run a `git status` is that it will say

```
# On branch master
```

You should think of the master branch as the place where your code lives in its purest, most production ready state. Git always creates the master branch with any new repo.

Here's a visual reference of the master branch, you'll see similar visual references along the unit:

![master-branch](.guides/img/master-branch.png)

Here are some guidelines

- you should avoid doing any development in the master branch
- don't create new features in the master branch
- don't fix bugs in the master branch, generally speaking you should create a new branch for each bug fix
- master is pure, master is holy and should be protected (maybe going a little far, just trying to make the point)