The previous challenge left the `conflict/conflict.txt` file with an "Automatic merge failed" conflict: 

```bash
<<<<<<< HEAD
Reproducing a Git conflict in order to understand it
=======
Reproducing a Git conflict to understand it
>>>>>>> feature
```

{Check It!|assessment}(test-2752899499)

|||guidance

### Correct answer:

1. Keep only the `Reproducing a Git conflict in order to understand it` text in the `conflict.txt` file
2. `git add -A` `git commit -m "Conflict solved: left master"`

|||