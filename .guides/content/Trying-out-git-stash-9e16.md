Let’s try this out in your Codio Box.

First of all, make sure you have committed everything so we have a clean starting point. 

1. Add a new file called `temp.txt` in the `temp/` folder
1. Stage this file `git add temp/temp.txt` for a single file or `git add -A` if you have have several new files
1. Run `git stash` to stash your changes

As soon as you run the `git stash` command, you will notice that `temp.txt` has disappeared together with the `temp/` directory. This is because `git stash` caused Git to roll back to the previous commit, where `temp.txt` did not exist. 

Regarding the `temp/` directory, Git omits adding/staging empty folders, so no empty directories will be pushed to the remote repo. The _deletion_ behaviour is `git stash` removing the whole path to the previously staged file: `git add temp/temp.txt`.

We can check what is on the stack with `git stash list`. You should see something like

```
stash@{0}: WIP on master: a0d2647 commit message shown here  
```

This would now enable you do switch to another branch in a clean state and without your dirty code changes.

Once you get back to your `master` branch, you can bring the stashed changes back with

```
git stash pop
```

You’ll notice that `temp.txt` has been restored and you are back to the exact same state you were before you stashed.

```
git stash list
```

will show that there is nothing more on the stack. If you had not popped the stash from the stack then you would see your stashed work listed.
