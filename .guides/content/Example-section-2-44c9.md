One day you wake up and decide to work on a new feature in your code. It will probably take you a week or two to build this feature. Bursting with developer passion, you start coding away. 

5 days later, someone reports an urgent bug that is threatening your product. The problem is that your code now has a half developed feature that means the whole thing is completely unstable and so fixing your bug is not possible.

With branching, you solve this issue by doing each feature and each bug fix in its own **branch**. Of course, you could fix 3 or 4 bugs in a single branch but to be really structured it is better to do one per branch.

You happen to be in your master branch, which is the only place you know about at this stage and is where all your work has been done until now.

Branching allows you to branch off from a clean starting point into a separate space where you can code without affecting your master branch.