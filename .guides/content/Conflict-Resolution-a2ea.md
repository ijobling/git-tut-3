Git is very clever at merging branches. As long as it is confident that it can merge not just files but also file contents together, it will do so.

However, if the same file has been edited in the same place in two branches undergoing a merge, then Git raises the white flag and will ask you to resolve the conflict yourself.

Let’s create this situation and deal with it. We’ll do this by creating a new branch

`git branch conflict`

Now switch to that branch and edit the poem in the `mary.txt` file by adding ‘blah blah’ to the end of the first line. Also, add the following verse to the end of the poem

```
It followed her to school one day
School one day, school one day
It followed her to school one day
Which was against the rules.
```

Commit these changes (you should know how by now) and then switch back to the `master` branch (you may see the _Branch is ahead_ message once again).

`git checkout master`

Now, in the master branch add ‘waffle’ to the end of the first line of the poem. Commit these changes, too.

We’ve now created a situation where the same line in a file has been edited in both branches. Let’s merge in the changes we made in the `conflict` branch with 

`git merge conflict`

Git realizes it can’t resolve this on its own, so you will see the following output in the terminal

```
Auto-merging mary.txt
CONFLICT (content): Merge conflict in mary.txtAutomatic merge failed; fix conflicts and then commit the result. 
```

If you run `git status` you’ll also see warnings there.

Now look at mary.txt and you can see the actual conflict embedded into the first verse of the poem

```
<<<<<<< HEAD
Mary had a little lamb, waffle
=======
Mary had a little lamb, blah blah
>>>>>>> conflct
Little lamb, little lamb,
Mary had a little lamb,
Its fleece was white as snow
```

What you now need to do is to resolve this conflict by editing the file. `<<<<< HEAD` means that everything from that point onwards until the `======` is contained within the branch you are trying to merge into (`master`).

Lower down, you’ll see `>>>>> conflict`, so everything above that up to the ‘======‘ is contained within the `conflict` branch.

All you now have to do is to edit the file to remove the section you don’t want to keep. If we want to keep the changes in the `conflict` branch, it will look like this after editing

```
Mary had a little lamb, blah blah
His fleece was white as snow,
And everywhere that Mary went,
The lamb was sure to go.
```

Although we have fixed the correction, we now have to perform a commit in order to store it properly

`git commit -am 'fixed the conflict'`