In the previous unit you learned about GitHub, an online platform for Git to store your local files in a remote repository. 

### Remote repositories 

You learned that in order to add a new remote repo you must: 

1. Have a GitHub account
2. Create a new empty repository in GitHub
3. Execute the `git remote add <remote-name> <remote-URL>` command in the correspondent local Git repo to connect both local and remote repositories

### Remote names

The most common `<remote-name>` is `origin` and so we can _push_ the local changes to the remote by doing a `git push origin master` where `origin` is a reference to the `<remote-URL>` and `master` is the branch that holds the latest edits (we will cover branches in this unit).

### Pulling changes

It is possible to download a remote repo by doing a `git pull origin master` or doing a `git clone` which Codio does for you when you create a new project and paste the remote GitHub repo URL.