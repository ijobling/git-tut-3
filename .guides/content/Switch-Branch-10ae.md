The reason we already added some files to the master branch is that I want to show that when you switch to a new branch (checkout a branch in Git parlance), Git will move all uncommitted changes from tracked and untracked files into your new branch. 

Let’s have a go

```
git checkout sonnets
```

Now get a list of all branches again

```
git branch
```

```
  master
* sonnets
```

Visual reference:

![sonnets-active](.guides/img/sonnets-branch-active.png)

We can see we're now on the `sonnets` branch. In Codio, you can also see the current branch by looking at the top of the Codio file tree, where you’ll see the branch name in parentheses next to the project name `git-tut-3 (sonnets)`.

## Commit what we brought across
Feel free to add more sonnets or leave things as they are. Either way, stage (`git add`) and commit the changes in your new `sonnets` branch.

If you perform a `git log` after committing, you'll see your commit message and your username at the top of the terminal screen.