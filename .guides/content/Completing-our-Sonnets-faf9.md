[Click here to close files opened in previous section](close_file mary.txt)

Now, let’s get back to our sonnets. By now you are hopefully comfortable with the process, so I’ll just give you the high level tasks.

First, checkout your `sonnets` branch then add each of the following Sonnets as files in the `sonnets/` directory then stage and commit each one individually. It is not the end of the world if you don’t, but we want to end up as close to the video as possible.

Paste in a new `sonnet-116.txt` file:

```
SONNET 116
Let me not to the marriage of true minds
Admit impediments. Love is not love
Which alters when it alteration finds,
Or bends with the remover to remove:
O no; it is an ever-fixed mark, 
That looks on tempests, and is never shaken;
It is the star to every wandering bark,
Whose worth's unknown, although his height be taken.
Love's not Time's fool, though rosy lips and cheeks 
Within his bending sickle's compass come; 
Love alters not with his brief hours and weeks, 
But bears it out even to the edge of doom.
   If this be error and upon me proved,
   I never writ, nor no man ever loved. 
```

Paste in a new `sonnet-19.txt` file:

```
SONNET 19
Devouring Time, blunt thou the lion's paws,
And make the earth devour her own sweet brood;
Pluck the keen teeth from the fierce tiger's jaws,
And burn the long-lived phoenix in her blood; 
Make glad and sorry seasons as thou fleet'st, 
And do whate'er thou wilt, swift-footed Time, 
To the wide world and all her fading sweets; 
But I forbid thee one most heinous crime: 
O, carve not with thy hours my love's fair brow,
Nor draw no lines there with thine antique pen; 
Him in thy course untainted do allow 
For beauty's pattern to succeeding men. 
Yet, do thy worst, old Time: despite thy wrong,
My love shall in my verse ever live young. 
```