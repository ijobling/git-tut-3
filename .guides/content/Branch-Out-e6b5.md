|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file sonnets/sonnets.txt)

|||

I mentioned that Git will help you out if you start your modifications in the master branch. If we now create a new branch, Git will take everything over from your working directory, including untracked and unstaged files.

```
git branch sonnets
```

You won’t see anything happen on the screen, but now enter the following

```
git branch
```

and you’ll see you have two branches. 

```
* master
  sonnets
```

Visual reference:

![sonnets-branch-inactive](.guides/img/sonnets-branch-inactive.png)

You are currently on the `master` branch as it has the `*` character next to it but you can see the `sonnets` branch has been successfully created.

If you now were to perform Git operations such as `git add` and `git commit`, they would be added to the `master` branch, as we have not yet switched over to the ‘sonnets’ branch.