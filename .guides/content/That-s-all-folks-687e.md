You are now more familiar with how Git branches work. 

In the next unit, you'll learn how to look up old commits in the Git history logs.