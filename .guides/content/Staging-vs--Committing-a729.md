The behaviour of `git checkout sonnets` to `git checkout master` after having committed the changes in the `sonnets` branch is that the `sonnets.txt` file dissapeared in the `master` branch. Why?

It is important to remember that when you execute a `git add file.txt` or `git add -A` to add many files, Git is only recognizing that the files have been created, deleted or modified. This _recognition_ process is called: _staging_. 

`git commit -m "commit message"` on the other hand, is when Git actually _records_ or creates the snapshots of the working directory to backup the local repository.

`git add` and `git commit` work very close in the way that it is not possible to commit any changes that have not been staged first. 