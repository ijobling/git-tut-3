Merging takes a specified branch and merges it into the current branch.

In our example, we want to merge *into* the `master` branch, so make sure you are in the `master` branch (`git branch` will tell you). 

Now we merge in our `fixv3` branch with

```
git merge fixv3
```

Now, take a look at `mary.txt`. You can see that the typo is fixed.