Now that we have merged everything in, we can remove the other branches. This is done with 

```
git branch -d sonnets
git branch -d fixv3
```