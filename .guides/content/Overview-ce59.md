## Branching

Branching is one of Git's most powerful features. Working in branches allows you to fully isolate each bug or feature you’re working on from your master branch and from other features and bug fixes you may be working on.

We’ll work through a couple of use cases shortly, but first let’s understand the reasons why branching is needed.
