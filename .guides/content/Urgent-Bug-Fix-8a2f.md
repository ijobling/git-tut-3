You get an urgent message that someone has discovered a nasty bug that needs immediate attention. You’re in the middle of working on your new sonnets but we can use Git branching.

The first thing we want to do is to go back to the clean code state that we would expect to find in the master branch.

**Make sure all your changes are committed in the `sonnets` branch** then let’s switch back to the master branch

```
git checkout master
```

Look what’s happened to `sonnets.txt`. It’s gone. That’s because it was not there in the master branch, meaning that we didn't commit the `sonnets.txt` file in the master branch but in the `sonnets` branch, so Git didn't record its existance in the master branch.

But `sonnets.txt` is safely in the `sonnets` branch of course.

You can switch back and forth between `master` and `sonnets` but end up in `master`.

The `sonnets` branch has effectively been parked until we want to go back to it.