[Click here to close files opened in previous section](close_file mary.txt)

{Check It!|assessment}(test-1127122384)

---
Fix the conflict in the next challenge.

|||guidance

Make sure you are on the `master` branch and: 

1. `git branch feature`
2. The `conflict/conflict.txt` file should have this text:
```
Reproducing a Git conflict in order to understand it
```
3. `git add -A`, `git commit -m "Any message"`, `git checkout feature`
4. The `conflict/conflict.txt` file should have this text:
```
Reproducing a Git conflict to understand it
```
5. `git add -A`, `git commit -m "Any message"`, `git checkout master`
6. `git merge feature`

|||