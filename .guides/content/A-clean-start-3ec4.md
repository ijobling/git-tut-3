The project in this unit comes with a simple project set up and with some commits already made, so everything is fully prepared for you.

Enter the following on the command line in your terminal to see the commits already made at the project status.
```
git log
git status
```

In the `git log` screen, you may see commit messages, long commit id's and other github usernames.

### Quiting the `git log` screen

In order to quit the `git log` screen, just type the `q` key. 