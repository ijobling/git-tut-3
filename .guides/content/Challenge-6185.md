{Check It!|assessment}(test-349672587)

|||guidance

### Correct answers:

1. Create the file `merge.txt` inside the `merge` folder and add the word: _Thaipoe_
2. `git add -A`, `git commit -m "Added merge.txt file"`
3. `git branch merge`, `git checkout merge`


|||